#!/usr/bin/env bash

echo "Starting to Scrape... wish us luck..."
node ./tools/scrape.js
echo "Scraping is complete... running compiler..."
node ./tools/compile.js
echo "All finished up! Output can be found in ./tools/data/output.csv"
