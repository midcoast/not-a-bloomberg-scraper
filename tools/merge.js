const fs = require( "fs" );
const request = require( "request" );
const path = require( "path" );
const _ = require( "lodash" );
const cj = require( "csvjson" );
const output = fs.readFileSync( path.join( __dirname, "data/output.csv" ), { encoding : "utf8" } );
const outputArr = json2array( cj.toObject( output ) );
const src = fs.readFileSync( path.join( __dirname, "data/source.csv" ), { encoding : "utf8" } );
const srcArr = json2array( cj.toObject( src ) );

function json2array(json){
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function(key){
        result.push(json[key]);
    });
    return result;
}

// console.log( srcArr );
// console.log( outputObj );

let mergedCompanies = srcArr.map( srcCo => {
  let twin = outputArr.find( outputCo => outputCo.Ticker === srcCo.Ticker );

  return Object.assign( {}, srcCo, twin );
} );

// console.log( mergedCompanies );

let csv = cj.toCSV( mergedCompanies, {
  delimiter: ",",
  headers: "key",
  wrap: false
} );

fs.writeFile( "tools/data/merged.csv", csv, ( err ) => {
  if (err) console.log( err );
} );
