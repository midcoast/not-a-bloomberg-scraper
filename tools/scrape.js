const fs = require( "fs" );
const path = require( "path" );
const Apify = require( "apify" );
const _ = require( "lodash" );
const cj = require( "csvjson" );

const apiKey = `69307750-eb0d-457b-b5fe-75c76ba1ecb6`;

const input = fs.readFileSync( path.join( __dirname, "data/source.csv" ), { encoding : "utf8" } );
const inputObj = cj.toObject( input );

// console.log( input );

Apify.main( async () => {
  let data = _.uniqBy( inputObj, "Ticker" );
  let mappedData = data.map( company => {
    let ticker = company.Ticker.split( " " );

    return {
      ...company,
      "BBTicker": `${ticker[0]}:${ticker[1]}`,
    };
  } );

  const launchPuppeteerOptions = {
    headless: true,
    proxyUrl: "http://auto:PgDWTZu5PvnfFj28zJfXvyHTt@proxy.apify.com:8000",
    // slowMo: 1000,
  };

  const requestList = new Apify.RequestList( {
    sources: []
  } );

  let i = 0;

  mappedData.forEach( ( company ) => {
    if( i < 5 ) {
      let id = encodeURIComponent( company.BBTicker );

      requestList.sources.push({ url: `https://www.bloomberg.com/markets2/api/datastrip/${id}` });
      requestList.sources.push({ url: `https://www.bloomberg.com/markets2/api/report/income/EQT/${id}/annual?currency=${company["Native Currency"]}` });
    }

    i++;
  } );

  await requestList.initialize();

  const crawler = new Apify.PuppeteerCrawler( {
    launchPuppeteerOptions,
    requestList,
    maxRequestRetries: 3,
    handlePageFunction: async ( { page, response, request } ) => {
      let company, mergedCompany;

      const dataset = await Apify.openDataset( "companies" );

      console.log( `Processing ${request.url}...` );

      let url = request.url.split("%3A");
      let ticker = url[0].split("/").pop();
      let code = url[1].split("/").shift();
      let compiledTicker = `${ticker}:${code}`;

      // filter out the company that we want
      company = _.filter( mappedData, ( co ) => {
        return co.BBTicker == `${compiledTicker}`;
      } );

      let json = await response.json();
      if( await !json.revenue ) json = json[0];

      await console.log( `Writing ${compiledTicker}...` );

      mergedCompany = _.merge( company[0], json );

      await dataset.pushData( mergedCompany );
    },
    handleFailedRequestFunction: async ( { request } ) => {
      // console.log( await request.errorMessages );
      console.log( `Oops, we are going to have to retry this one: ${request.url}` );
    }
  } );

  await crawler.run();
});
