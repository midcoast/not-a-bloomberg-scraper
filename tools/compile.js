const fs = require( "fs" );
const request = require( "request" );
const path = require( "path" );
const _ = require( "lodash" );
const input = require( "require-all" )( path.join( __dirname, "../apify_storage/datasets/companies" ) );
const cj = require( "csvjson" );

const companies = _.map( input, ( c ) => {
  let scrapeValues = {
      "Sorting Codes": c["Sorting Codes"],
      "Ticker": c["Ticker"],
      "Name": c["Name"],
      "Native Currency": c["Native Currency"],
      "ISIN": c["ISIN"],
      "BBID": _.get( c, "bbid" ),
      "BICS Industry": _.get( c, "bicsIndustry" ),
      "GICS Sector": _.get( c, "gicsSector" ),
      "GICS Industry": _.get( c, "gicsIndustry" ),
      "GICS Industry Group": c["GICS Industry Group"],
      "GICSLevel4": c["GICSLevel4"],
      "Income": c.income && c.income.length ? c.income.pop().value * 1000000 : 0,
      "Revenue": c.revenue && c.revenue.length ? c.revenue.pop().value * 1000000 : 0,
      "Profit": c.profit && c.profit.length ? c.profit.pop().value : 0,
      "Market Cap": _.get( c, "marketCap" ) || 0,
  };

  return { ...c, ...scrapeValues };
} );

let mergedCompanies = _( companies )
  .groupBy( "Ticker" )
  .map( _.spread( _.assign ) )
  .value();

let options = {
  method: 'GET',
  url: 'http://data.fixer.io/api/latest?access_key=0ef3369f0e1ca2bb3dd0ea6650ddabcb',
  json: true,
};

request( options, ( error, response, body ) => {
  if ( error ) {
    console.log( error );
  }

  console.log( body );

  let convertedCompanies = mergedCompanies.map( co => {
    let curr = co["Currency"];
    let getUsd = ( amt ) => ( ( amt ) / body.rates[curr] ) * body.rates["USD"];

    if ( curr !== "USD" ) {
      co["Income USD"] = getUsd( co["Income"] );
      co["Revenue USD"] = getUsd( co["Revenue"] );
      co["Market Cap USD"] = getUsd( co["Market Cap"] );
    }
    else {
      co["Income USD"] = co["Income"];
      co["Revenue USD"] = co["Revenue"];
      co["Market Cap USD"] = co["Market Cap"];
    }

    return co;
  } );

  let csv = cj.toCSV( convertedCompanies, {
    delimiter: ",",
    headers: "key",
    wrap: false
  } );

  fs.writeFile( "tools/data/output.csv", csv, ( err ) => {
    if (err) console.log( err );
  } );
} );
