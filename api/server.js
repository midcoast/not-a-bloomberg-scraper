const express = require( "express" );
const request = require( "request" );
const Apify = require( "apify" );
const cors = require( "cors" );

const { puppeteer } = Apify.utils;

const server = express();
const apiKey = "69307750-eb0d-457b-b5fe-75c76ba1ecb6";

server.use( cors() );

server.set( "port", process.env.PORT || 8000);

server.get( "/", ( req, res ) => {
  res.send( "Hello! You probably need to hit an appropriate route to use this app." );
} );

// Return companies from Open FIGI search query
// Requires search string (q)
server.get( "/companies", ( req, res ) => {
  let query = req.query.query;
  let exchCode = req.query.code;

  var options = {
    method: 'POST',
    url: 'https://api.openfigi.com/v2/search',
    json: true,
    body: { query, exchCode, securityType: "Common Stock" }
  };

  if (apiKey) {
    options.headers = { 'X-OPENFIGI-APIKEY': apiKey };
  }

  request( options, ( error, response, body ) => {
    if ( error ) {
      console.log( error );
    }

    res.send( body );
  } );
} );

// Get revenue and marketcap from Bloomberg
// Requires ticker and market (IBM, US)
server.get( "/company", async ( req, res ) => {
  try {
    let ticker = req.query.ticker;
    let market = req.query.market;
    let data = {};

    const launchPuppeteerOptions = {
      headless: true,
      proxyUrl: "http://auto:PgDWTZu5PvnfFj28zJfXvyHTt@proxy.apify.com:8000"
    };

    const requestList = new Apify.RequestList( {
        sources: [
          { url: `https://www.bloomberg.com/markets2/api/datastrip/${ticker}%3A${market}` },
          { url: `https://www.bloomberg.com/markets2/api/report/income/EQT/${ticker}%3A${market}/annual?locale=en&currency=USD` }
        ]
    } );

    await requestList.initialize();

    const crawler = new Apify.PuppeteerCrawler( {
      launchPuppeteerOptions,
      requestList,
      handlePageFunction: async ( { page, response, request } ) => {
        console.log( `Processing ${request.url}...` );

        let pageData = await response.json();

        console.log( pageData );

        if ( pageData.revenue ) {
          data[ "annuals" ] = pageData;
        }
        else {
          data[ "fundamentals" ] = pageData[0];
        }
      },
      handleFailedRequestFunction: async ( { request } ) => {
        console.log( await request.errorMessages );
      },
    } );

    await crawler.run()
      .then( () => {
        console.log( data );

        res.status( 200 ).send( data );
      } )
      .catch( ( err ) => {
        console.log( err );
      } );

    console.log( "Done." );
  }
  catch ( err ) {
    console.log( err );
  }
} );

server.on('unhandledRejection', async (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason);
});

server.listen( process.env.PORT || 8000, () => {
  console.log( "\nThis is definitely not an API for scraping Bloomberg...\nAPI running on port 8000.\n" );
} );
